#ifndef DOUBLER_H
#define DOUBLER_H

const int FACTOR = 2;

int doubler(int x);

#endif /* DOUBLER_H */
